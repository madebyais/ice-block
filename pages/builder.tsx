import {FC, useState} from "react";
import BlockBuilder from "components/blocks/builder";

export default function BuilderPage() {
  const [blocks, setBlocks] = useState([
    {
      type: 'Image',
      metadata: {
        url: 'https://via.placeholder.com/150'
      }
    },
    {
      type: 'Paragraph',
      metadata: {
        text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. '
      }
    }
  ])

  return (
    <div className={`container mx-auto p-10`}>
      {/* @ts-ignore */}
      <BlockBuilder blocks={blocks} setBlocks={setBlocks} />
    </div>
  )
}
