import {BlockTypes} from "./types";
import {HeadingSchema} from "./commons/heading";
import {ParagraphSchema} from "./commons/paragraph";
import {ImageSchema} from "./commons/image";
import {TeamMemberSchema} from "./company/team-member";

export default function getBlockSchema(type: BlockTypes): any {
  let schema: any

  switch (type) {
    case BlockTypes.Heading:
      schema = HeadingSchema
      break
    case BlockTypes.Image:
      schema = ImageSchema
      break
    case BlockTypes.Paragraph:
      schema = ParagraphSchema
      break
    case BlockTypes.TeamMember:
      schema = TeamMemberSchema
      break
  }

  return schema
}