import {IBlockProps} from "./interface";
import {BlockTypes} from "./types";
import {MetadataDummyHeading, MetadataDummyImage, MetadataDummyParagraph} from "./commons";
import {MetadataDummyTeamMember} from "./company";

const generateDefaultMetadata = (blockType: string): IBlockProps => {
  let type: BlockTypes = BlockTypes.Unknown
  let metadata: any = {}

  switch (blockType) {
    case BlockTypes.Heading:
      type = BlockTypes.Heading
      metadata = MetadataDummyHeading
      break
    case BlockTypes.Image:
      type = BlockTypes.Image
      metadata = MetadataDummyImage
      break
    case BlockTypes.Paragraph:
      type = BlockTypes.Paragraph
      metadata = MetadataDummyParagraph
      break
    case BlockTypes.TeamMember:
      type = BlockTypes.TeamMember
      metadata = MetadataDummyTeamMember
      break
  }

  return { type, metadata }
}

export default generateDefaultMetadata