import {BlockTypes} from "./types";

export interface IBlockProps {
  type: BlockTypes
  metadata: any
}

export interface IBlockBuilderProps {
  blocks: Array<IBlockProps>
  setBlocks: Function
}

export interface IBlockContainerProps {
  index: number
  setPosition: Function
}

export interface IBlockSelectorProps {
  blocks: Array<IBlockProps>
  setBlocks: Function
  setVisibleBlockSelectorDrawer: Function
}