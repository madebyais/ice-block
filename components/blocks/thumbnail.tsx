import {FC, MouseEventHandler, useState} from "react";
import {BlockTypes} from "./types";
import generateDefaultMetadata from "./metadata";
import BlockGenerator from "./generator";
import {Select} from "antd";

const categories: Array<string> = [
  'All',
  'Common',
  'Company'
]

const defaultAllBlocks: Array<any> = [
  { section: 'section', category: 'common', title: 'Common' },
  { section: 'block', category: 'common', title: 'Heading', ...generateDefaultMetadata(BlockTypes.Heading) },
  { section: 'block', category: 'common', title: 'Image', ...generateDefaultMetadata(BlockTypes.Image) },
  { section: 'block', category: 'common', title: 'Paragraph', ...generateDefaultMetadata(BlockTypes.Paragraph) },

  { section: 'section', category: 'company', title: 'Company' },
  { section: 'block', category: 'company', title: 'Team Member', ...generateDefaultMetadata(BlockTypes.TeamMember) },
]

interface IBlockThumbnailProps {
  title: string
  block?: any
  onClick?: MouseEventHandler
}

const BlockThumbnail: FC<IBlockThumbnailProps> = ({ title, block, onClick }) => (
  <div className={`border p-3 flex justify-center items-center cursor-pointer`} onClick={onClick}>
    <div className={`flex flex-col justify-between items-center`}>
      {block}
      <div className={`mt-5 font-semibold text-center`}>{title}</div>
    </div>
  </div>
)

const BlockThumbnails = ({ onClick }: any) => {
  const [filteredBlocks, setFilteredBlocks] = useState<Array<any>>(defaultAllBlocks)
  const onChangeCategory = (category: string) => {
    if (category == 'all') setFilteredBlocks(defaultAllBlocks)
    else setFilteredBlocks(defaultAllBlocks.filter(b => b.category == category))
  }
  return (
    <div className={`grid grid-cols-1 md:grid-cols-4 gap-5`}>
      <div className={`col-span-1`}>
        <Select defaultValue={`all`} onChange={onChangeCategory} className={`w-full`}>
          {categories.map((category, i) => (
            <Select.Option key={i} value={category.toLowerCase()}>{category}</Select.Option>
          ))}
        </Select>
      </div>
      {filteredBlocks.map((b, i) => (
        <>{b.section == 'section' ? (
            <h2 key={i} className={`md:col-span-4 font-bold text-lg`}>{b.title}</h2>
          ):(
            <BlockThumbnail key={i} title={b.title} block={BlockGenerator(b)} onClick={e => onClick(b.type)} />
          )}</>
      ))}
    </div>
  )
}

export default BlockThumbnails