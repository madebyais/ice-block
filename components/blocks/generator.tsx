import {FC} from "react";
import Heading from "components/blocks/commons/heading";
import Paragraph from "components/blocks/commons/paragraph";
import {BlockTypes} from "components/blocks/types";
import Image from "components/blocks/commons/image";
import TeamMember from "components/blocks/company/team-member";

interface IGeneratorProps {
  type: BlockTypes
  metadata: any
}

const BlockGenerator: FC<IGeneratorProps> = (props) => {
  let block: any

  switch (props.type) {
    case BlockTypes.Heading:
      block = <Heading {...props} />
      break
    case BlockTypes.Image:
      block = <Image {...props} />
      break
    case BlockTypes.Paragraph:
      block = <Paragraph {...props} />
      break
    case BlockTypes.TeamMember:
      block = <TeamMember {...props} />
      break
    default:
      block = <>unknown block type</>
  }

  return <>{block}</>
}

export default BlockGenerator