import {FC, useEffect, useState} from "react";
import ITeamMemberProps from "./interface";
import {getDefaultString} from "util/default";
import Fetch from "util/fetch";
import Loading from "components/icon/loader";

const TeamMember: FC<ITeamMemberProps> = ({ metadata, children }) => {
  const [isLoading, setIsLoading] = useState<boolean>(false)
  const [teamMembers, setTeamMembers] = useState<Array<any>>([])

  let className: string = getDefaultString(metadata?.class, '')

  const getTeamMembers = async (apiUrl: string) => {
    setIsLoading(true)
    const resp = await Fetch('GET', apiUrl)
    setTeamMembers(resp.data)
    setIsLoading(false)
  }

  useEffect(() => {
    if (metadata.apiUrl) getTeamMembers(metadata?.apiUrl || '').then(r => {})
    if (metadata.members && metadata.members.length > 0) setTeamMembers(metadata.members)
  }, [metadata.apiUrl, metadata.members])

  if (isLoading && teamMembers.length == 0) return <Loading />
  if (!isLoading && teamMembers.length == 0) return <>Please add team members or use external API.</>

  return (
    <div className={`grid grid-col-1 md:grid-cols-2 gap-5 ${className}`}>
      {teamMembers.map((member, i) => (
        <div key={i} className={`flex flex-col justify-center items-center`}>
          <div className={`mb-3`}>
            {/*eslint-disable-next-line @next/next/no-img-element*/}
            <img className={`w-32 h-32 rounded-full`} src={member.avatar} alt={member.firstName} />
          </div>
          <div className={`text-lg`}>{member.firstName} {member.lastName}</div>
          <div>{member.email}</div>
        </div>
      ))}
    </div>
  )
}

export default TeamMember

// @ts-ignore
export {default as TeamMemberSchema} from './schema.yaml'