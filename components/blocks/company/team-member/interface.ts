export interface TeamMemberMetadata {
  apiUrl?: string
  members?: Array<any>
  class?: string
}

export default interface ITeamMemberProps {
  metadata: TeamMemberMetadata
}