export interface ParagraphMetadata {
  text: string
  alignment: string
  class?: string
}

export default interface IParagraphProps {
  metadata: ParagraphMetadata
}