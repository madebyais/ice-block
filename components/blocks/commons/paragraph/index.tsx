import {FC} from "react";
import IParagraphProps from "./interface";
import {getDefaultString} from "../../../../util/default";

const Paragraph: FC<IParagraphProps> = ({ metadata, children }) => {
  let className: string = getDefaultString(metadata?.class, '')
  let text: string = getDefaultString(metadata?.text, '')
  let alignment: string = getDefaultString(metadata?.alignment, '')

  return <p className={`${className} ${alignment}`} dangerouslySetInnerHTML={{ __html: text}} />
}

export default Paragraph

// @ts-ignore
export {default as ParagraphSchema} from './schema.yaml'