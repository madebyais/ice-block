export enum HeadingTypes {
  H1 = "H1",
  H2 = "H2",
  H3 = "H3",
  H4 = "H4",
  H5 = "H5"
}

export interface HeadingMetadata {
  type: HeadingTypes
  text: string
  size?: string
  weight?: string
  class?: string
}

export default interface IHeadingProps {
  metadata: HeadingMetadata
}