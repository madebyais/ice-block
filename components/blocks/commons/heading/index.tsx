import {FC, useEffect} from "react";
import IHeadingProps, {HeadingTypes} from "./interface";
import {getDefaultString} from "../../../../util/default";

const Heading: FC<IHeadingProps> = ({ metadata = {}, children }) => {
  let component: any
  let text: string = getDefaultString(metadata?.text, '')
  let size: string = getDefaultString(metadata?.size, 'text-md')
  let weight: string = getDefaultString(metadata?.weight, 'font-normal')

  let className: string = getDefaultString(metadata?.class, '')
  className = `${size} ${weight} ${className}`

  switch (metadata.type) {
    case HeadingTypes.H1:
      component = <h1 className={className}>{text}</h1>
      break
    case HeadingTypes.H2:
      component = <h2 className={className}>{text}</h2>
      break
    case HeadingTypes.H3:
      component = <h3 className={className}>{text}</h3>
      break
    case HeadingTypes.H4:
      component = <h3 className={className}>{text}</h3>
      break
    case HeadingTypes.H5:
      component = <h3 className={className}>{text}</h3>
      break
  }

  return <>{component}</>
}

export default Heading

// @ts-ignore
export {default as HeadingSchema} from './schema.yaml'