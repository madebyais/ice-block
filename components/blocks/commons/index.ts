// @ts-nocheck
export {default as MetadataDummyHeading} from 'components/blocks/commons/heading/metadata-dummy.yaml'
export {default as MetadataDummyImage} from 'components/blocks/commons/image/metadata-dummy.yaml'
export {default as MetadataDummyParagraph} from 'components/blocks/commons/paragraph/metadata-dummy.yaml'