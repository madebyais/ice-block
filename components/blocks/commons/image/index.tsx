import {FC} from "react";
import {getDefaultString} from "util/default";
import IImageProps from "./interface";

const Image: FC<IImageProps> = ({ metadata, children }) => {
  let className: string = getDefaultString(metadata?.class, '')
  let url: string = getDefaultString(metadata?.url, '')
  let alt: string = getDefaultString(metadata?.alt, '')

  // eslint-disable-next-line @next/next/no-img-element
  return <img src={url} alt={alt} className={className} />
}

export default Image

// @ts-ignore
export {default as ImageSchema} from './schema.yaml'