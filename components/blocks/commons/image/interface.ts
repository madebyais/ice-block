export interface ImageMetadata {
  url: string
  alt?: string
  class?: string
}

export default interface IImageProps {
  metadata: ImageMetadata
}