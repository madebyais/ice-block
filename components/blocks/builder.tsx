import {FC, useRef, useState} from "react";
import {IBlockBuilderProps, IBlockProps, IBlockContainerProps} from "./interface";
import {HTML5Backend} from "react-dnd-html5-backend";
import {DndProvider, DropTargetMonitor, useDrag, useDrop} from "react-dnd";
import {XYCoord} from "dnd-core";
import BlockGenerator from "./generator";
import {Button, Drawer, Dropdown, Menu} from "antd";
import getBlockSchema from "./schema";
// @ts-ignore
import Form from "@rjsf/antd";
import BlockSelector from "./selector";

const BlockBuilder: FC<IBlockBuilderProps> = ({ blocks, setBlocks }) => {
  const [formData, setFormData] = useState<any>({})
  const [formSchema, setFormSchema] = useState<any>(undefined)
  const [uiSchema, setUiSchema] = useState<any>(undefined)
  const [visibleEditDrawer, setVisibleEditDrawer] = useState<boolean>(false)
  const [currentEditIndex, setCurrentEditIndex] = useState<number>(-1)
  const [visibleBlockSelectorDrawer, setVisibleBlockSelectorDrawer] = useState<boolean>(false)

  const setBlockPosition = (srcIndex: number, destIndex: number) => {
    let tempBlock1: IBlockProps = blocks[srcIndex]
    let tempBlock2: IBlockProps = blocks[destIndex]

    let newBlocks: Array<IBlockProps> = [...blocks]
    newBlocks[srcIndex] = tempBlock2
    newBlocks[destIndex] = tempBlock1

    setBlocks(newBlocks)
  }

  const onClickEditBtn = (index: number, block: IBlockProps) => {
    const schema: any = getBlockSchema(block.type)
    setCurrentEditIndex(index)
    setFormSchema(schema.form)
    setUiSchema(schema.ui)
    setFormData(block.metadata)
    setVisibleEditDrawer(true)
  }

  const onClickDuplicateBtn = (index: number, block: IBlockProps) => {
    let newBlocks: Array<IBlockProps> = blocks.map(b => b)
    newBlocks.splice(index, 0, block)
    setBlocks(newBlocks)
  }

  const onClickRemoveBtn = (index: number) => {
    let newBlocks: Array<IBlockProps> = [...blocks]
    newBlocks.splice(index, 1)
    setBlocks(newBlocks)
  }

  const onSubmitEditForm = (values: any) => {
    let newBlocks: Array<IBlockProps> = JSON.parse(JSON.stringify(blocks))
    newBlocks[currentEditIndex].metadata = {...newBlocks[currentEditIndex].metadata, ...values.formData}
    setVisibleEditDrawer(false)
    setCurrentEditIndex(-1)
    setBlocks(newBlocks)
  }

  const onCancelBtnDrawer = () => {
    setVisibleEditDrawer(false)
    setFormData({})
    setFormSchema(undefined)
    setUiSchema(undefined)
  }

  return (
    <div className={`space-y-3`}>
      <DndProvider backend={HTML5Backend}>
        {blocks.map((block: IBlockProps, index: number) => (
          <BlockContainer key={index} index={index} setPosition={setBlockPosition}>
            <div className={`flex cursor-pointer`}>
              <div className={`mr-5`}>
                <Dropdown.Button overlay={
                  <Menu>
                    <Menu.Item key={1} onClick={e => onClickEditBtn(index, block)}>Edit</Menu.Item>
                    <Menu.Item key={2} onClick={e => onClickDuplicateBtn(index, block)}>Duplicate</Menu.Item>
                    <Menu.Divider />
                    <Menu.Item key={3} onClick={e => onClickRemoveBtn(index)} danger>Remove</Menu.Item>
                  </Menu>
                } placement={`bottomCenter`} />
              </div>
              <div className={`w-full flex items-center`}>
                <BlockGenerator {...block} />
              </div>
            </div>
          </BlockContainer>
        ))}
      </DndProvider>

      <div className={`flex justify-center pt-14`}>
        <Button onClick={e => setVisibleBlockSelectorDrawer(true)}>+ Add block</Button>
      </div>

      <Drawer
        visible={visibleEditDrawer}
        width={500}
        onClose={onCancelBtnDrawer}>
          <br/>
          {
            formSchema &&
              <Form
                schema={formSchema}
                uiSchema={uiSchema}
                formData={formData}
                onSubmit={onSubmitEditForm}>
                  <div className={`space-x-2`}>
                    <Button type={`primary`} htmlType={`submit`}>Save</Button>
                    <Button onClick={onCancelBtnDrawer}>Cancel</Button>
                  </div>
              </Form>
          }
      </Drawer>

      <Drawer
        visible={visibleBlockSelectorDrawer}
        placement={`bottom`} height={500}
        onClose={e => setVisibleBlockSelectorDrawer(false)}>
          <BlockSelector blocks={blocks} setBlocks={setBlocks} setVisibleBlockSelectorDrawer={setVisibleBlockSelectorDrawer} />
      </Drawer>
    </div>
  )
}

const BlockContainer: FC<IBlockContainerProps> = ({ index, setPosition, children }) => {
  const BLOCK = 'BLOCK'
  interface item {
    index: number
  }

  const ref = useRef<HTMLDivElement>(null)
  const [{ handlerId }, drop] = useDrop({
    accept: BLOCK,
    collect(monitor) {
      return { handlerId: monitor.getHandlerId() }
    },
    hover(item: item, monitor: DropTargetMonitor) {
      if (!ref.current) return
      const dragIndex = item.index
      const hoverIndex = index
      if (dragIndex == hoverIndex) return
      const hoverBoundingRect = ref.current?.getBoundingClientRect()
      const hoverMiddleY = (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2
      const clientOffset = monitor.getClientOffset()
      const hoverClientY = (clientOffset as XYCoord).y - hoverBoundingRect.top
      if (dragIndex < hoverIndex && hoverClientY < hoverMiddleY) return
      if (dragIndex > hoverIndex && hoverClientY > hoverMiddleY) return
      setPosition(dragIndex, hoverIndex)
      item.index = hoverIndex
    }
  })

  const [{ isDragging }, drag] = useDrag({
    type: BLOCK,
    item: () => {
      return { index }
    },
    collect: (monitor: any) => ({
      isDragging: monitor.isDragging(),
    }),
  })

  const opacity = isDragging ? 0.3 : 1
  drag(drop(ref))
  return (
    <div
      ref={ref}
      data-handler-id={handlerId}
      style={{opacity}}>
        {children}
    </div>
  )
}

export default BlockBuilder