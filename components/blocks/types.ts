export enum BlockTypes {
  Unknown = 'Unknown',
  Heading = 'Heading',
  Image = 'Image',
  Paragraph = 'Paragraph',

  TeamMember = 'Team Member'
}