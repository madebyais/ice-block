import {FC, MouseEventHandler} from "react";
import {IBlockProps, IBlockSelectorProps} from "./interface";
import {BlockTypes} from "./types";
import generateDefaultMetadata from "./metadata";
import Heading from "components/blocks/commons/heading";
import Image from "components/blocks/commons/image";
import Paragraph from "components/blocks/commons/paragraph";
import TeamMember from "./company/team-member";
import BlockThumbnails from "./thumbnail";

const BlockSelector: FC<IBlockSelectorProps> = ({ blocks, setBlocks, setVisibleBlockSelectorDrawer }) => {
  const onClick = (type: string) => {
    let block: IBlockProps = generateDefaultMetadata(type)
    let newBlocks: Array<IBlockProps> = [...blocks, block]
    setBlocks(newBlocks)
    setVisibleBlockSelectorDrawer(false)
  }

  return (
    <div>
      <BlockThumbnails onClick={onClick} />
      {/*<h2 className={`md:col-span-4 font-bold text-lg`}>Commons</h2>*/}
      {/*<BlockThumbnail title={`Heading`} onClick={e => onClick(BlockTypes.Heading)} block={<Heading metadata={generateDefaultMetadata(BlockTypes.Heading).metadata} />} />*/}
      {/*<BlockThumbnail title={`Image`} onClick={e => onClick(BlockTypes.Image)} block={<Image metadata={generateDefaultMetadata(BlockTypes.Image).metadata} />} />*/}
      {/*<BlockThumbnail title={`Paragraph`} onClick={e => onClick(BlockTypes.Paragraph)} block={<Paragraph metadata={generateDefaultMetadata(BlockTypes.Paragraph).metadata} />} />*/}

      {/*<h2 className={`md:col-span-4 font-bold text-lg`}>Company</h2>*/}
      {/*<BlockThumbnail title={`Team Member`} onClick={e => onClick(BlockTypes.TeamMember)} block={<TeamMember metadata={generateDefaultMetadata(BlockTypes.TeamMember).metadata} />} />*/}
    </div>
  )
}

interface IBlockThumbnailProps {
  title: string
  block?: any
  onClick?: MouseEventHandler
}

const BlockThumbnail: FC<IBlockThumbnailProps> = ({ title, block, onClick }) => (
  <div className={`border p-3 flex justify-center items-center cursor-pointer`} onClick={onClick}>
    <div className={`flex flex-col justify-between items-center`}>
      {block}
      <div className={`mt-5 font-semibold text-center`}>{title}</div>
    </div>
  </div>
)

export default BlockSelector