export const getDefaultString = (value: any, defaultValue: string): string => value ? value : defaultValue
export const getDefaultNumber = (value: number, defaultValue: number): number => value ? value : defaultValue
export const getDefaultBoolean = (value: boolean, defaultValue: boolean): boolean => value ? value : defaultValue
export const getDefaultObject = (value: any, defaultValue: any): object => value ? value : defaultValue